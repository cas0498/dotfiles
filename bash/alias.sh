###############################################################################
# Aliases
# Inspired by Mark H. Nichols of Zanish.net
###############################################################################

alias bashrc="$EDITOR $DOTFILES/bash/.bashrc"
