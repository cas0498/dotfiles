#!/bin/bash
#####################################################
#  Watchguard Video Specific Functions and Aliases  #
#####################################################

export DEV1_USER=csmith
export DEV1_USER_DIR=/fast/users/${DEV1_USER}

function holo
{
    [ -d "${PROJECTS}"/holocron ] && cl "${PROJECTS}"/holocron/
}

function amba
{
    [ -d "${PROJECTS}"/meta-ambarella ] && cl "${PROJECTS}"/meta-ambarella
}

function wgv
{
    [ -d "${PROJECTS}"/meta-wgv ] && cl "${PROJECTS}"/meta-wgv
}

function runit
{
    [ -d "${PROJECTS}"/meta-runit ] && cl "${PROJECTS}"/meta-runit
}
