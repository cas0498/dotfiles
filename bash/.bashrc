# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
    *) return;;
esac

case $HOSTNAME in
    "dev1") exec zsh; return;;
    "dev2") exec zsh; return;;
    "dev3-wgv") exec zsh; return;;
    "dev4-wgv") exec zsh; return;;
esac

# Path
[[ -d $HOME/bin ]] && export PATH="$HOME/bin:$PATH"
[[ -d $HOME/.local/bin ]] && export PATH="$HOME/.local/bin:$PATH"

# Dotfile locations
[[ -d $HOME/.config ]] && export XDG_CONFIG_HOME="$HOME/.config"
[[ -d $HOME/.config/dotfiles ]] && export DOTFILES="$HOME/.config/dotfiles"
[[ -d $HOME/Project ]] && export PROJECTS="$HOME/Projects"

[[ -n $XDG_CONFIG_HOME ]] && export HISTFILE=$XDG_CONFIG_HOME/.bash_history

export home=$HOME
export LC_COLLATE=C

[[ -r $DOTFILES/bash/checks.sh ]] && source $DOTFILES/bash/checks.sh
[[ $HAS_GIT -eq 1 ]] && [[ -r $DOTFILES/bash/git.sh ]] && source $DOTFILES/bash/git.sh
[[ -r $DOTFILES/bash/colors.sh ]] && source $DOTFILES/bash/colors.sh
[[ -r $DOTFILES/bash/themes/af-magic.sh-theme ]] && source $DOTFILES/bash/themes/af-magic.sh-theme
[[ -r $DOTFILES/bash/alias.sh ]] && source $DOTFILES/bash/alias.sh
[[ -r $DOTFILES/bash/watchguard.sh ]] && source $DOTFILES/bash/watchguard.sh

if [[ $HAS_FZF -eq 1 ]]
then
    [[ -r $DOTFILES/bash/directories.sh ]] && source $DOTFILES/bash/directories.sh
    [[ -r $DOTFILES/.fzf.bash ]] && source $DOTFILES/.fzf.bash
    export FZF_CTRL_T_COMMAND='find ${1:-.} -type f'
    export FZF_CTRL_T_OPTS='--height 80%
                            --border
                           --select-1
                            --exit-0'
                            # --preview="[[ $(file --mime {}) =~ binary ]] &&
                            #   echo {} is a binary file ||
                            #   (highlight -O ansi -l {})
                            #   2> /dev/null | head -50"
  export FZF_ALT_C_COMMAND='find ${1:-.} -type d'
  export FZF_ALT_C_OPTS="--height 80%"
                         # --preview 'tree -C {}'"
  export FZF_CTRL_R_OPTS="--sort"
fi
