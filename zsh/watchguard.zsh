#####################################################
#  Watchguard Video Specific Functions and Aliases  #
#####################################################

# Aliases
alias frmt="find . -regex '.*\.\(cpp\|h\|cc\|cxx\)' -exec clang-format -style=file -i {} \; "
alias grev="git review"
alias coverage="sshfs dev2:/fast/users/csmith/holocron/builds_g2wear_out/x86_64-coverage/coverage /Volumes/Projects/coverage"

alias gchogm='git checkout gerrit/master'
alias gchogg='git checkout gerrit/msi_gatesgarth'
alias gfg='git checkout gerrit/master; git fetch --all --prune; git rebase gerrit/master'
alias gfgg='git checkout gerrit/msi_gatesgarth; git fetch --all --prune; git rebase gerrit/msi_gatesgarth'
alias grbgm='git rebase gerrit/master'
alias grbgg='git rebase gerrit/msi_gatesgarth'

# Exports
export DEV1_USER=csmith
export DEV1_USER_DIR=/fast/users/csmith

# Functions
function proj() {
    [[ -d $PROJECTS ]] && cl $PROJECTS
}

function holo() {
    [[ -d $PROJECTS/holocron ]] && cl $PROJECTS/holocron
}

function amba() {
    [[ -d $PROJECTS/meta-ambarella ]] && cl $PROJECTS/meta-ambarella
}

function wgv() {
    [[ -d $PROJECTS/meta-wgv ]] && cl $PROJECTS/meta-wgv
}

function runit() {
    [[ -d $PROJECTS/meta-runit ]] && $PROJECTS/meta-runit
}

function s5l() {
    [[ -d $PROJECTS/Ambarella_S5L_SDK ]] && $PROJECTS/Ambarella_S5L_SDK
}

function s6l() {
    [[ -d $PROJECTS/Ambarella_S6L_SDK ]] && $PROJECTS/Ambarella_S6L_SDK
}

function dsync() {
    ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/holocron/ && git clean -f"
    rsync -v -r -a --exclude 'builds_g2wear_out' \
        --exclude 'logs' \
        --exclude '.active_project' \
        -e ssh $PROJECTS/holocron/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/holocron"
}

function abuild() {
    dsync
    ssh "${DEV1_USER}@dev3" "cd ${DEV1_USER_DIR}/holocron && BUILDER_DOCKER_RUN_OPTS='--dns=10.81.90.1 --dns-search=watchguardvideo.local' && ./script/build-aarch64-debug.sh --enable-test-filter $@"
}

function dbuild() {
    dsync
    ssh "${DEV1_USER}@dev3" "cd ${DEV1_USER_DIR}/holocron && BUILDER_DOCKER_RUN_OPTS='--dns=10.81.90.1 --dns-search=watchguardvideo.local' && ./script/build-x86_64-debug.sh --enable-test-filter $@"
}

function aninja() {
    dsync
    ssh "${DEV1_USER}@dev3" "cd ${DEV1_USER_DIR}/holocron && BUILDER_DOCKER_RUN_OPTS='--dns=10.81.90.1 --dns-search=watchguardvideo.local' && ./script/build-aarch64-debug.sh --compile-only $@"
}

function dninja() {
    dsync
    ssh "${DEV1_USER}@dev3" "cd ${DEV1_USER_DIR}/holocron && BUILDER_DOCKER_RUN_OPTS='--dns=10.81.90.1 --dns-search=watchguardvideo.local' && ./script/build-x86_64-debug.sh --compile-only $@"
}

function d2sync() {
    ssh ${DEV1_USER}@dev2 "cd ${DEV1_USER_DIR}/holocron && git clean -f"
    rsync -v -r -a --exclude 'builds_g2wear_out' \
        --exclude 'logs' \
        --exclude '.active_project' \
        -e ssh $PROJECTS/holocron/ "${DEV1_USER}@dev2:${DEV1_USER_DIR}/holocron"
}

function d2ninja() {
    d2sync
    ssh "${DEV1_USER}@dev2" "cd ${DEV1_USER_DIR}/holocron && BUILDER_DOCKER_RUN_OPTS='--dns=10.81.90.1 --dns-search=watchguardvideo.local' && ./script/build-x86_64-debug.sh --compile-only"
}

function d2build() {
    d2sync
    ssh "${DEV1_USER}@dev2" "cd ${DEV1_USER_DIR}/holocron && BUILDER_DOCKER_RUN_OPTS='--dns=10.81.90.1 --dns-search=watchguardvideo.local' && ./script/build-x86_64-debug.sh --enable-test-filter $@"
}

function tisync() {
    ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/extractor/poky/meta-ti-wgv/ && git clean -f"
    ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/RecoveryUtils && git clean -f"
    rsync -v -r -a \
        -e ssh $PROJECTS/meta-ti-wgv/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/extractor/poky/meta-ti-wgv"
    rsync -v -r -a \
        -e ssh $PROJECTS/RecoveryUtils/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/RecoveryUtils"
}

function ambasync() {
    ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/Ambarella_S5L_SDK && git clean -f"
    rsync -v -r -a \
        -e ssh $PROJECTS/Ambarella_S5L_SDK/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/Ambarella_S5L_SDK"
}

function csync() {
    case $1 in
        amba)
            ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/CRUCIBLE/tmpout-vg700_msi_gatesgarth/meta-ambarella/ && git clean -f"
            rsync -v -r -a \
                -e ssh $PROJECTS/meta-ambarella/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/CRUCIBLE/tmpout-vg700_msi_gatesgarth/meta-ambarella"
            ;;
        wgv)
            ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/CRUCIBLE/tmpout-vg700_msi_gatesgarth/meta-wgv/ && git clean -f"
            rsync -v -r -a \
                -e ssh $PROJECTS/meta-wgv/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/CRUCIBLE/tmpout-vg700_msi_gatesgarth/meta-wgv"
            ;;
        *)
            ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/CRUCIBLE/holocron/ && git clean -f"
            rsync -v -r -a --exclude 'builds_g2wear_out' \
                --exclude 'logs' \
                --exclude '.active_project' \
                -e ssh $PROJECTS/holocron/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/CRUCIBLE/holocron"
            ;;
    esac
}

function cninja() {
    csync
    ssh "${DEV1_USER}@dev3" "cd ${DEV1_USER_DIR}/CRUCIBLE/holocron && BUILDER_DOCKER_RUN_OPTS='--dns=10.81.90.1 --dns-search=watchguardvideo.local' && ./script/build-x86_64-debug.sh --compile-only $@"
}

function cbuild() {
    csync
    ssh "${DEV1_USER}@dev3" "cd ${DEV1_USER_DIR}/CRUCIBLE/holocron && BUILDER_DOCKER_RUN_OPTS='--dns=10.81.90.1 --dns-search=watchguardvideo.local' && ./script/build-x86_64-debug.sh --enable-test-filter $@"
}

function v7sync() {
    case $1 in
        amba)
            ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/CRUCIBLE/tmpout-v700_msi_gatesgarth/meta-ambarella/ && git clean -f"
            rsync -v -r -a \
                -e ssh $PROJECTS/meta-ambarella/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/CRUCIBLE/tmpout-v700_msi_gatesgarth/meta-ambarella"
            ;;
        wgv)
            ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/CRUCIBLE/tmpout-v700_msi_gatesgarth/meta-wgv/ && git clean -f"
            rsync -v -r -a \
                -e ssh $PROJECTS/meta-wgv/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/CRUCIBLE/tmpout-v700_msi_gatesgarth/meta-wgv"
            ;;
        *)
            ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/CRUCIBLE/holocron/ && git clean -f"
            rsync -v -r -a --exclude 'builds_g2wear_out' \
                --exclude 'logs' \
                --exclude '.active_project' \
                -e ssh $PROJECTS/holocron/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/CRUCIBLE/holocron"
            ;;
    esac
}

function v3sync() {
    case $1 in
        amba)
            ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/CRUCIBLE/tmpout-v300_master/meta-ambarella/ && git clean -f"
            rsync -v -r -a \
                -e ssh $PROJECTS/meta-ambarella/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/CRUCIBLE/tmpout-v300_master/meta-ambarella"
            ;;
        wgv)
            ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/CRUCIBLE/tmpout-v300_master/meta-wgv/ && git clean -f"
            rsync -v -r -a \
                -e ssh $PROJECTS/meta-wgv/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/CRUCIBLE/tmpout-v300_master/meta-wgv"
            ;;
        *)
            ssh ${DEV1_USER}@dev3 "cd ${DEV1_USER_DIR}/CRUCIBLE/holocron/ && git clean -f"
            rsync -v -r -a --exclude 'builds_g2wear_out' \
                --exclude 'logs' \
                --exclude '.active_project' \
                -e ssh $PROJECTS/holocron/ "${DEV1_USER}@dev3:${DEV1_USER_DIR}/CRUCIBLE/holocron"
            ;;
    esac
}
