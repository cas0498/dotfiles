SAVEHIST=10000
alias history='fc -fl 1'

# fh - repeat history
fh() {
  print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) \
    | fzf +s --tac --height 40% | sed 's/ *[0-9]* *//')
}
