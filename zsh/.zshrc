# path
[[ -d /opt/local/libexec/gnubin ]] && PATH=/opt/local/libexec/gnubin:$PATH
[[ -d /opt/local/bin ]] && PATH=/opt/local/bin:$PATH
[[ -d /opt/local/sbin ]] && PATH=/opt/local/sbin:$PATH
[[ -d /usr/local/sbin ]] && PATH=/usr/local/sbin:$PATH
[[ -d /opt/homebrew/bin ]] && PATH=/opt/homebrew/bin:$PATH
[[ -d /opt/homebrew/sbin ]] && PATH=/opt/homebrew/sbin:$PATH
[[ -d $HOME/.config/.cargo/bin ]] && PATH=$HOME/.config/.cargo/bin:$PATH
[[ -d $HOME/.local/bin ]] && PATH=$HOME/.local/bin:$PATH
[[ -d $HOME/.local/.fzf/bin ]] && PATH=$HOME/.local/.fzf/bin:$PATH
export PATH

# sources
[[ -f $ZDOTDIR/checks.zsh ]] && source $ZDOTDIR/checks.zsh
[[ -f $ZDOTDIR/setopt.zsh ]] && source $ZDOTDIR/setopt.zsh
[[ -f $ZDOTDIR/zsh_hooks.zsh ]] && source $ZDOTDIR/zsh_hooks.zsh

if [[ $HAS_GIT -eq 1 ]]
then
  [[ -f $ZDOTDIR/git.zsh ]] && source $ZDOTDIR/git.zsh
fi

[[ -f $ZDOTDIR/colors.zsh ]] && source $ZDOTDIR/colors.zsh
[[ -f $ZDOTDIR/themes/af-magic.zsh-theme ]] && source $ZDOTDIR/themes/af-magic.zsh-theme
[[ -f $ZDOTDIR/completion.zsh ]] && source $ZDOTDIR/completion.zsh
[[ -f $ZDOTDIR/alias.zsh ]] && source $ZDOTDIR/alias.zsh
[[ -f $ZDOTDIR/functions.zsh ]] && source $ZDOTDIR/functions.zsh
[[ -f $ZDOTDIR/search.zsh ]] && source $ZDOTDIR/search.zsh
[[ -f $ZDOTDIR/keybindings.zsh ]] && source $ZDOTDIR/keybindings.zsh
[[ -f $ZDOTDIR/tmux.zsh ]] && source $ZDOTDIR/tmux.zsh
[[ -f $ZDOTDIR/watchguard.zsh ]] && source $ZDOTDIR/watchguard.zsh

if [[ $HAS_FZF -eq 1 ]]
then
  [[ -f $DOTFILES/.fzf.zsh ]] && source $DOTFILES/.fzf.zsh
  export FZF_CTRL_T_COMMAND='find ${1:-.} -type f'
  export FZF_CTRL_T_OPTS='--height 80%
                          --border
                          --preview="[[ $(file --mime {}) =~ binary ]] &&
                            echo {} is a binary file ||
                            (highlight -O ansi -l {})
                            2> /dev/null | head -50"
                          --select-1
                          --exit-0'
  export FZF_ALT_C_COMMAND='find ${1:-.} -type d'
  export FZF_ALT_C_OPTS="--height 80%
                         --preview 'tree -C {}'"
  export FZF_CTRL_R_OPTS="--sort"
fi

[[ -f $ZDOTDIR/directories.zsh ]] && source $ZDOTDIR/directories.zsh
[[ -f $ZDOTDIR/history.zsh ]] && source $ZDOTDIR/history.zsh

[[ -n "${ZDOTDIR}" ]] && export HISTFILE=$ZDOTDIR/.zhistory
[[ -n "${DOTFILES}" ]] && export LESSHISTFILE="/dev/null"
