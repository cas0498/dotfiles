# Dotfile locations
[[ -d $HOME/.config/dotfiles ]] && export DOTFILES=$HOME/.config/dotfiles
[[ -d $HOME/.config/dotfiles/zsh ]] && export ZDOTDIR=$HOME/.config/dotfiles/zsh
[[ -d /opt/local/share/nvim ]] && export VIM=/opt/local/share/nvim
[[ -d /opt/local/share/nvim/runtime/ ]] && export VIMRUNTIME=/opt/local/share/nvim/runtime
[[ -f $DOTFILES/.vim/.vimrc ]] && export MYVIMRC=$DOTFILES/.vim/.vimrc
[[ -f $DOTFILES/.vim/.gvimrc ]] && export MYGVIMRC=$DOTFILES/.vim/.gvimrc

export home=$HOME
# zshcompdump location
export ZSH_COMPDUMP=$HOME/.config/dotfiles/misc/zcompdump

# Ipython locations
export IPYTHONDIR=$HOME/.config/dotfiles

# LS and LC
export LC_COLLATE=C

# Project Environments
[[ -d /Volumes/Projects ]] && export PROJECTS=/Volumes/Projects
[[ -d $HOME/.config/.cargo ]] && export CARGO_HOME=$HOME/.config/.cargo
[[ -d $HOME/.config/.rustup ]] && export RUSTUP_HOME=$HOME/.config/.rustup
