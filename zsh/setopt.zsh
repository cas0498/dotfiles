###############################################################################
# Inspired (and stolen from) Mark H. Nichols of Zanshin.net                   #
###############################################################################
## Basics
setopt no_beep # don't beep on error
setopt interactive_comments # allow comments in interactive shells

## Scripts and Functions
setopt multios # preform implicit tees and cats with mult redirects

## Changing Directories
setopt auto_cd # If it isn't a command, it's a directory, so go there
setopt auto_pushd
setopt pushd_ignore_dups # ignore duplicate dirs on stack
setopt pushdminus

## Expansion
setopt extended_glob # treat #, ~, and ^ as part of patters for file generation

## History
setopt append_history # allow multiple sessions to append to one zsh history
setopt extended_history # save timestamp and duration
setopt hist_expire_dups_first # trim oldest dups first
setopt hist_ignore_dups # ignore duplicate events
setopt hist_find_no_dups # ignore dups when searching history
setopt hist_reduce_blanks # remove blank space from cmd
setopt hist_verify # expand, don't exec
setopt share_history # imports new commands and appends typed commands

## Completion
setopt complete_in_word # allow completion from middle of word
setopt always_to_end # when completing from middle of word, move cursor to end
unsetopt menu_complete # do not autoselect first completion entry
unsetopt flowcontrol
setopt auto_menu # show completion menu on multiple tabs
setopt auto_name_dirs # any parameter set to the absolute name of a dir is that dir

## Correction
setopt correct # spelling corrections
setopt correct_all # correct arguments too

## Prompts
setopt prompt_subst # allow expansions in prompt
setopt transient_rprompt # only show rprompt on current prompt
