# tmux keyboard shortcuts
alias tnew='tmux -CC new -s'
alias tls='tmux ls'
alias tat='tmux -CC attach -t'
alias tswitch='tmux switch -t'
alias td='tmux detach'
alias tkill='tmux kill-session -t'
alias tconf='nvim $ZDOTDIR/.tmux.conf'
