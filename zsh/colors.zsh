# colors
autoload colors; colors

# ls colors
unset LS_COLORS
export CLICOLOR=1
export CLICOLOR_FORCE=1
export LS_COLORS="di=38;2;245;255;90:\
fi=38;2;209;106;255:\
ln=30:\
so=32:\
pi=33:\
ex=38;2;255;80;110:\
bd=34;46:\
cd=34;43:\
su=30;41:\
sg=30;46:\
tw=38;2;9;22;7;48;2;76;24;33:\
ow=30;43"
