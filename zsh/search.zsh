###############################################################################
# Search Aliases using Ag (The Silver Searcher)
###############################################################################
#
## Ag (The Silver Searcher)
alias ag="ag -C 4 --hidden --pager='less -iRX'"
alias agi="ag -i"
alias agg="ag -g"
