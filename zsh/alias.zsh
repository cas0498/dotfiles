###############################################################################
# Aliases
# Inspired by Mark H. Nichols of Zanish.net
###############################################################################
#

## Editor
if [[ $HAS_NVIM -eq 1 ]]
then
  export EDITOR='nvim'
else
  export EDITOR='vim'
fi

## Vim and MacVim Editor
alias zshrc="$EDITOR $ZDOTDIR/.zshrc"
alias bashrc="$EDITOR $DOTFILES/bash/.bashrc"
alias zshenv="$EDITOR $ZDOTDIR/.zshenv"
alias vimrc="$EDITOR $HOME/.config/nvim/init.lua"
alias gvimrc="$EDITOR $MYGVIMRC"
alias -s c=$EDITOR
alias -s h=$EDITOR
alias -s py=$EDITOR
alias -s c++=$EDITOR

## Quick compilation
alias cppcompile='c++ -std=c++11 -stdlib=libc++'

## System aliases
alias shut='sudo shutdown -h now'
alias re='sudo shutdown -r now'
alias sl='sudo shutdown -s now'

## SSH aliases
alias sconfig="$EDITOR $HOME/.ssh/config"

# Some useful nmap aliases for scan modes

# Nmap options are:
#  -sS - TCP SYN scan
#  -v - verbose
#  -T1 - timing of scan. Options are paranoid (0), sneaky (1), polite (2), normal (3), aggressive (4), and insane (5)
#  -sF - FIN scan (can sneak through non-stateful firewalls)
#  -PE - ICMP echo discovery probe
#  -PP - timestamp discovery probe
#  -PY - SCTP init ping
#  -g - use given number as source port
#  -A - enable OS detection, version detection, script scanning, and traceroute (aggressive)
#  -O - enable OS detection
#  -sA - TCP ACK scan
#  -F - fast scan
#  --script=vulscan - also access vulnerabilities in target

alias nmap_open_ports="nmap --open"
alias nmap_list_interfaces="nmap --iflist"
alias nmap_slow="nmap -sS -v -T1"
alias nmap_fin="nmap -sF -v"
alias nmap_full="nmap -sS -T4 -PE -PP -PS80,443 -PY -g 53 -A -p1-65535 -v"
alias nmap_check_for_firewall="nmap -sA -p1-65535 -v -T4"
alias nmap_ping_through_firewall="nmap -PS -PA"
alias nmap_fast="nmap -F -T5 --version-light --top-ports 300"
alias nmap_detect_versions="nmap -sV -p1-65535 -O --osscan-guess -T4 -Pn"
alias nmap_check_for_vulns="nmap --script=vulscan"
alias nmap_full_udp="nmap -sS -sU -T4 -A -v -PE -PS22,25,80 -PA21,23,80,443,3389 "
alias nmap_traceroute="nmap -sP -PE -PS22,25,80 -PA21,23,80,3389 -PU -PO --traceroute "
alias nmap_full_with_scripts="sudo nmap -sS -sU -T4 -A -v -PE -PP -PS21,22,23,25,80,113,31339 -PA80,113,443,10042 -PO --script all "
alias nmap_web_safe_osscan="sudo nmap -p 80,443 -O -v --osscan-guess --fuzzy "

## STOP USING RM!
if [[ $IS_MAC ]]
then
    alias rm='trash'
    alias update="brew update && brew upgrade && brew cleanup && neofetch"
elif [[ $IS_LINUX ]]
then
    if [[ $HAS_APT ]]
    then
        alias aptinst="sudo apt -y install"
        alias aptup="sudo apt update && sudo apt -y upgrade"
    fi
    if [[ $HAS_APTITUDE ]]
    then
        unalias aptinst
        unalias aptup
        alias aptinst="sudo aptitude -y install"
        alias aptup="sudo aptitude update && sudo aptitude -y upgrade"
        alias aptpurge="sudo aptitude purge '~c'"
    fi
    if [[ $HAS_DNF ]]
    then
       alias dnf="sudo dnf -y"
       alias dni="sudo dnf -y install"
       alias dnu="sudo dnf update"
    fi
fi

if [[ $HAS_PIP ]]; then
    alias p2l="pip list"
    alias p2lo="pip list --outdated"
    alias p2i="pip install"
    alias p2iu="pip install --upgrade"
fi

if [[ $HAS_PIP3 ]]
then
    alias pipl="pip3 list"
    alias piplo="pip3 list --outdated"
    alias pipi="pip3 install"
    alias pipiu="pip3 install --upgrade"
fi

alias slack="slack-term --config $home/snap/slack-term/current/slack-term.json"
alias slack-wgv="slack-term --config $home/snap/slack-term/current/wgv-slack-term.json"
