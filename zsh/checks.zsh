# checks (stolen from zshuery and zanshin.net)
if [[ $OSTYPE = linux* ]]; then
    export IS_LINUX=1
fi

if [[ $OSTYPE = darwin* ]]; then
    export IS_MAC=1
fi

if [[ -d $DOTFILES/.fzf || `command -v fzf` ]]; then
    export HAS_FZF=1
fi

if [[ -x `command -v git` ]]; then
    export HAS_GIT=1
fi

if [[ -x `command -v pip` ]]; then
    export HAS_PIP=1
fi

if [[ -x `command -v pip3` ]]; then
    export HAS_PIP3=1
fi

# Figure out the SHORT hostname
if [[ $IS_MAC -eq 1 ]]; then
    # macOS's $HOST changes with dhcp, etc. Use ComputerName if possible.
    export SHORT_HOST=$(scutil --get ComputerName 2>/dev/null) || export SHORT_HOST=${HOST/.*/}

    if [[ -x `command -v brew` ]]; then
        export HAS_BREW=1
    fi

    if [[ -x `command -v port` ]]; then
        export HAS_PORT=1
    fi
else
    export SHORT_HOST=${HOST/.*/}

    if [[ -x `command -v apt-get` ]]; then
        export HAS_APT=1
    elif [[ -x `command -v dnf` ]]; then
        export HAS_DNF=1
    elif [[ -x `command -v aptitude` ]]; then
        export HAS_APTITUDE=1
    fi

    if [[ $TILIX_ID || $VTE_VERSION ]]
    then
        source /etc/profile.d/vte.sh
    fi
fi

if [[ -e $ZDOTDIR/.iterm2_shell_integration.`basename $SHELL` ]]
then
    source $ZDOTDIR/.iterm2_shell_integration.`basename $SHELL`
fi

# NEOVIM
if [[ -x `command -v nvim` ]]
then
    export HAS_NVIM=1
fi
