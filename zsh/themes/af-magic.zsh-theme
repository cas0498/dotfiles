# af-magic.zsh-theme
# Repo: https://github.com/andyfleming/oh-my-zsh
# Direct Link: https://github.com/andyfleming/oh-my-zsh/blob/master/themes/af-magic.zsh-theme

if [ $UID -eq 0 ]; then NCOLOR="red"; else NCOLOR="green"; fi
return_code="%(?..%{[38;2;255;80;110m%}%? ↵%{$reset_color%})"

# primary prompt
PROMPT='%{[38;2;96;98;102m%}------------------------------------------------------------%{$reset_color%}
%{[38;2;135;106;255m%}%3~\
$(git_prompt_info) \
%{[38;2;255;156;90m%}%(!.#.») %{$reset_color%}'
PROMPT2='%{[38;2;255;80;110m%}\ %{$reset_color%}'
RPS1='${return_code}'

# right prompt
RPROMPT='%{[38;2;96;98;102m%}%m %D{%a %d.%b.%Y %R}%{$reset_color%}%'

# git settings
ZSH_THEME_GIT_PROMPT_PREFIX="%{[38;2;135;106;255m%}(%{[38;2;90;255;156m%}"
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_DIRTY="%{[38;2;255;132;50m%}*%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{[38;2;135;106;255m%})%{$reset_color%}"
