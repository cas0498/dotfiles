# Changing/making/removing directory
alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'
alias -g ......='../../../../..'

alias -- -='cd -'
alias 1='cd -'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias 7='cd -7'
alias 8='cd -8'
alias 9='cd -9'

alias md='mkdir -p'
alias rd=rmdir
# alias d='dirs -v | head -10'

if [[ $HAS_FZF -eq 1 ]]
then
    function d() {
      local dir
      dir=`dirs -lp | fzf +m +s --height 40%`
      cl "$dir"
    }
fi

# List directory contents
function l() {
  if [[ $IS_MAC -eq 1 ]]; then
    if [[ $# -eq 0 ]]; then
      /usr/local/bin/gls -lLhF --color=always | more -RE;
    elif [[ $# -gt 1 ]]; then
      print 'Error'
    else
      /usr/local/bin/gls -lLhF --color=always $1 | more -RE;
    fi
  else
    if [[ $# -eq 0 ]]; then
      ls -lHLhF --color=always | less -RXE;
    elif [[ $# -gt 1 ]]; then
      print 'Error'
    else
      ls -lHLhF --color=always $1 | less -RXE;
    fi
  fi
}

function ll() {
  if [[ $IS_MAC -eq 1 ]]; then
    if [[ $# -eq 0 ]]; then
      /usr/local/bin/gls -lALhF --color=always | more -RXE;
    elif [[ $# -gt 1 ]]; then
      print 'Error'
    else
      /usr/local/bin/gls -lALhF --color=always $1 | more -RXE;
    fi
  else
    if [[ $# -eq 0 ]]; then
      ls -lAHLhF --color=always | less -RXE;
    elif [[ $# -gt 1 ]]; then
      print 'Error'
    else
      ls -lAHLhF --color=always $1 | less -RXE;
    fi
  fi
}

function cl() {
  if [[ $IS_MAC -eq 1 ]]; then
    if [[ $# -eq 0 ]]; then
      cd ~; l
    elif [[ $# -gt 1 ]]; then
      print 'Error'
    else
      cd $1; l
    fi
  else
    if [[ $# -eq 0 ]]; then
      cd ~; l
    elif [[ $# -gt 1 ]]; then
      print 'Error'
    else
      cd $1; l
    fi
  fi
}

function cll() {
  if [[ $IS_MAC -eq 1 ]]; then
    if [[ $# -eq 0 ]]; then
      cd ~; ll
    elif [[ $# -gt 1 ]]; then
      print 'Error'
    else
      cd $1; ll
    fi
  else
    if [[ $# -eq 0 ]]; then
      cd ~; ll
    elif [[ $# -gt 1 ]]; then
      print 'Error'
    else
      cd $1; ll
    fi
  fi
}

# Push and pop directories on directory stack
alias pu='pushd'
alias po='popd'
